package application.milkynet.project;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

/**
 * 
 * @author TheBestJB
 * Class reading and writing the config.properties file
 * This is the class to refer to get the settings
 * by calling its public static fields.
 *
 */
public class PropertiesManager {

	public static String HOME_URL;
	public static boolean manualCheckUpdate;
	
	/**
	 * Read the .properties file and sets the fields.
	 */
	public static void getProperties() {
		try {
			File f = new File("config.properties");
			if (f.exists()) {
				InputStream is = new FileInputStream(f);
				Properties prop = new Properties();
				prop.load(is);
				HOME_URL = prop.getProperty("HomeUrl");
				manualCheckUpdate = new Boolean(prop.getProperty("manualCheckUpdate"));
				is.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Writes the .properties file with the new settings.
	 * @param url
	 * @param updateCheck
	 */
	public static void setPropFromDialog(String url, boolean updateCheck) {
		try {
			URI uri = new URI(url);
			HOME_URL = uri.toString();
			manualCheckUpdate = updateCheck;
		} catch (URISyntaxException e1) {
			String validUrl = "http://"+url;
			HOME_URL = validUrl;
		}
		try {
			File f = new File("config.properties");
			Properties prop = new Properties();
			Writer wr = new FileWriter(f);
			prop.setProperty("HomeUrl", HOME_URL);
			prop.setProperty("manualCheckUpdate", Boolean.toString(manualCheckUpdate));
			prop.store(wr, null);
			wr.flush();
			wr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		getProperties();
	}
}
