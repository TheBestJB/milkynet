package application.milkynet.project;

import java.io.File;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

/**
 * 
 * @author TheBestJB
 * Main class, launching properties and setting the main Stage / Scene.
 *
 */
public class Main extends Application {
	
	public static String version = "0.5";
	public static Stage primaryStage;
	private BorderPane rootLayout;
	
	@Override
	public void start(Stage primaryStage) {
		PropertiesManager.getProperties();
		isConfigExists();
		Main.primaryStage = primaryStage;
		primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/img/MilkyNet32.png")));
		rootLayout();
		runOverview();
	}
	
	/**
	 * Sets the Scene as a BorderPane and gives it to the Stage.
	 */
	public void rootLayout() {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/rootLayout.fxml"));
		try {
			rootLayout = (BorderPane) loader.load();
			Scene scene = new Scene(rootLayout);
			scene.getStylesheets().add(Main.class.getResource("view/application.css").toString());
			primaryStage.setScene(scene);
			primaryStage.setTitle("MilkyNet");
			primaryStage.setMaximized(true);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Sets the center of the Scene (BorderPane) as an AnchorPane. WebEngine's room.
	 */
	public void runOverview() {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/Overview.fxml"));
		try {
			AnchorPane overview = loader.load();
			rootLayout.setCenter(overview);
			} catch (IOException e) {
			e.printStackTrace();
		}
		runUrlField();
	}
	
	/**
	 * Loads the navigation bar and sets it to the top of the Scene.
	 */
	public void runUrlField() {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/UrlField.fxml"));
		try {
			HBox urlField = loader.load();
			rootLayout.setTop(urlField);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Checks if the config.properties file exists.
	 * If not, instead of creating it, it sets a default home page.
	 */
	private void isConfigExists() {
		File f = new File("config.properties");
		if (!f.exists())
			PropertiesManager.HOME_URL = "http://www.google.fr";
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
