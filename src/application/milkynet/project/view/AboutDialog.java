package application.milkynet.project.view;

import java.io.IOException;

import application.milkynet.project.Main;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * 
 * @author TheBestJB
 * Loads the about dialog presenting the project.
 *
 */
public class AboutDialog {

	@FXML
	private Text versionText;
	@FXML
	private Text authorText;
	@FXML
	private Hyperlink linkHyp;
	
	private static Stage stage;
	
	@FXML
	private void initialize() {
		versionText.setText("MilkyNet - version : "+Main.version);
	}
	
	/**
	 * Sets the Stage and Scene.
	 */
	public void setAboutDial() {
		stage = new Stage();
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/AboutDialog.fxml"));
		try {
			BorderPane pane = loader.load();
			Scene scene = new Scene(pane);
			stage.setTitle("About");
			stage.initModality(Modality.WINDOW_MODAL);
			stage.initOwner(Main.primaryStage);
			stage.setResizable(false);
			stage.setScene(scene);
			stage.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Loads the link into the WebEngine and close the dialog.
	 */
	@FXML
	private void linkAction() {
		WebController.engine.load(linkHyp.getText());
		stage.close();
	}
}
