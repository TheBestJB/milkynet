package application.milkynet.project.view;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import application.milkynet.project.Main;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * 
 * @author TheBestJB
 * Sets the update Dialog and verifies if there is a more recent version.
 * Then offers to download it.
 *
 */
public class UpdateDialog {

	@FXML
	private ProgressIndicator prog;
	@FXML
	private Button yesButton;
	@FXML
	private Button noButton;
	@FXML
	private Text currentVText;
	@FXML
	private Text isUpToDateText;
	@FXML
	private Text downloadText;
	
	private static Stage stage;
	private Scene scene;
	private static String isUpText;
	public static boolean toUpdate;
	
	/**
	 * Firstly hide some Texts and Button while checking the last available version.
	 * Then if there is one, shows the downloading information.
	 */
	@FXML
	private void initialize() {
		isUpToDateText.setVisible(false);
		downloadText.setVisible(false);
		yesButton.setVisible(false);
		noButton.setVisible(false);
		currentVText.setText("Current version : "+Main.version);
		checkForUpdate();
		isUpToDateText.setText(isUpText);
		isUpToDateText.setVisible(true);
		prog.setVisible(false);
		if (toUpdate) {
			downloadText.setText("Download it ?");
			downloadText.setVisible(true);
			yesButton.setVisible(true);
			noButton.setVisible(true);
		}
	}
	
	/**
	 * Loads the fxml in the Scene.
	 */
	public void setUpdateDialog() {
		stage = new Stage();
		FXMLLoader loader = new FXMLLoader();
		try {
			loader.setLocation(Main.class.getResource("view/UpdateDialog.fxml"));
			BorderPane pane = loader.load();
			scene = new Scene(pane);
			initStage();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * The user wants to download the latest version (by pressing the "Yes" Button)
	 * Downloads it with the default browser.
	 */
	@FXML
	private void yesButtonAction() {
		stage.close();
		URL url;
		try {
			url = new URL("http://www.c4k-team.com/MilkyNet/MilkyNet.jar");
			Desktop.getDesktop().browse(url.toURI());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Close the dialog by pressing the "No" Button.
	 */
	@FXML
	private void noButtonAtion() {
		stage.close();
	}
	
	/**
	 * Opens a connection to read the version in a text file.
	 * Then sets the boolean "toUpdate" to determine if the current version is the latest.
	 */
	public static void checkForUpdate() {
		URL url;
		try {
			url = new URL("http://www.c4k-team.com/MilkyNet/version.txt");
			URLConnection connec = url.openConnection();
			BufferedReader input = new BufferedReader(new InputStreamReader(connec.getInputStream()));
			String latestV = input.readLine();
			if (Main.version.equals(latestV)) {
				isUpText = "This version is UP TO DATE !";
				toUpdate = false;
			}
			else {
				isUpText = "There is a most recent version : MilkyNet V"+latestV;
				toUpdate = true;
			}
			input.close();
				
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Sets Stage settings.
	 */
	private void initStage() {
		stage.setTitle("Check for updates");
		stage.setResizable(false);
		stage.initOwner(Main.primaryStage);
		stage.initModality(Modality.WINDOW_MODAL);
		stage.setScene(scene);
		stage.showAndWait();
	}
}
