package application.milkynet.project.view;

import application.milkynet.project.PropertiesManager;
import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 * 
 * @author TheBestJB
 * Class that sets a public static WebEngine, offering other classes to use the engine.
 *
 */
public class WebController {

	@FXML
	private WebView webView;
	
	public static WebEngine engine;
	
	/**
	 * When the WebEgine is initialized, it sets the public static WebEngine and loads the Home page.
	 */
	@FXML
	public void initialize() {
		engine = webView.getEngine();
		engine.load(PropertiesManager.HOME_URL);
	}
}
