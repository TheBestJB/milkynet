package application.milkynet.project.view;

import java.io.IOException;

import application.milkynet.project.Main;
import application.milkynet.project.PropertiesManager;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * 
 * @author TheBestJB
 * Creates the Options Dialog and provides its actions
 *
 */
public class OptionsDialog {

	@FXML
	private TabPane tabPane;
	@FXML
	private Tab HPTab;
	@FXML
	private Tab advancedTab;
	@FXML
	private TextField HPField;
	@FXML
	private Button OKButton;
	@FXML
	private Button CancelButton;
	@FXML
	private RadioButton manualUpdButton;
	@FXML
	private RadioButton autoUpdButton;
	
	public static final int HOME_TAB = 1;
	public static final int ADVANCED_TAB = 2;
	private static int tabToFocus;
	private static Tab focusedTab;
	private static Stage stage;
	
	/**
	 * Sets the url (default or user's preference) in the TextField - Home's Tab.
	 * Select the RadioButton (default or user's preference) - Advanced Tab.
	 * Verifies which tab is asked and selects it.
	 */
	@FXML
	private void initialize() {
		HPField.setText(PropertiesManager.HOME_URL);
		selectUpdateRButton();
		askedTab(tabToFocus);
		tabPane.getSelectionModel().select(focusedTab);
	}
	
	/**
	 * Sets the Stage and Scene's dialog and sets the tab's number
	 * to determine which tab was asked.
	 * @param i
	 */
	public void setOptionsDial(int i) {
		tabToFocus = i;
		initParam();
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/OptionsDialog.fxml"));
			
			AnchorPane pane = (AnchorPane) loader.load();
			Scene scene = new Scene(pane);
			stage.setScene(scene);
			stage.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * To press Enter when the Home Page TextField is focused is equivalent to OKAction.
	 */
	@FXML
	private void HPFieldEnterAction() {
		OKAction();
	}
	
	/**
	 * Sets the main Stage's settings.
	 */
	private void initParam() {
		stage = new Stage();
		stage.setTitle("Options");
		stage.initModality(Modality.WINDOW_MODAL);
		stage.initOwner(Main.primaryStage);
		stage.setResizable(false);
	}
	
	/**
	 * Checks if the settings have been modified.
	 * If yes, asks to PropertiesManager to write them.
	 */
	@FXML
	private void OKAction() {
		boolean manualUpdate = manualUpdButton.isSelected() ? true : false;
		
		if (!HPField.getText().equals(PropertiesManager.HOME_URL) 
				|| manualUpdate != PropertiesManager.manualCheckUpdate)
			PropertiesManager.setPropFromDialog(HPField.getText(), manualUpdate);
		
		stage.close();
	}
	
	/**
	 * Close the dialog without saving changes.
	 */
	@FXML
	private void CancelAction() {
		stage.close();
	}
	
	/**
	 * Select the asked tab.
	 * @param i
	 */
	private void askedTab(int i) {
		switch (i) {
		case 1:
			focusedTab = HPTab;
			break;
			
		case 2:
			focusedTab = advancedTab;
			break;

		default:
			focusedTab = HPTab;
			break;
		}
	}
	
	/**
	 * Selects the (default or user's preference) right radio button.
	 */
	private void selectUpdateRButton() {
		if (PropertiesManager.manualCheckUpdate)
			manualUpdButton.setSelected(true);
		else
			autoUpdButton.setSelected(true);
	}
}
