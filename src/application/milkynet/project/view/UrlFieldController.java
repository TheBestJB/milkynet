package application.milkynet.project.view;

import javafx.scene.control.Label;
import application.milkynet.project.Main;
import application.milkynet.project.PropertiesManager;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Paint;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebHistory.Entry;

/**
 * 
 * @author TheBestJB
 * Class adding all elements of the navigation bar
 * plus gives the action or listeners methods.
 *
 */
public class UrlFieldController {

	@FXML
	private TextField currentURL;
	@FXML
	private Button HomeButton, GoButton, NextButton, PrevButton;
	@FXML
	private MenuButton OptionsButton;
	@FXML
	private MenuItem aboutMI; 
	@FXML
	private MenuItem setHPMI;
	@FXML
	private MenuItem updateMI;
	@FXML
	private Label updateLab;
	@FXML
	private Button reloadButton;
	@FXML
	private MenuItem advancedMI;
	
	private WebEngine engine;
	private ObservableList<Entry> HistoryEntries;
	private boolean isGoButton;
	private ChangeListener<String> engineListener;
	private ChangeListener<String> fieldListener;
	private ChangeListener<Number> HistoryIndexListener;
	private int indexHistoryValue;
	private AboutDialog aboutDial;
	private UpdateDialog updateDial;
	private Image homeImage = new Image(Main.class.getResourceAsStream("/img/Home.png"));
	private Image reloadImage = new Image(Main.class.getResourceAsStream("/img/reload.png"));
	
	/**
	 * Launches some listeners, gets the WebEngine history,
	 * and eventually check the latest version.
	 */
	@FXML
	public void initialize() {
		HomeButton.setGraphic(new ImageView(homeImage));
		reloadButton.setGraphic(new ImageView(reloadImage));
		engine = WebController.engine;
		HistoryEntries = engine.getHistory().getEntries();
		currentURL.setText(PropertiesManager.HOME_URL);
		setEngineListener();
		setFieldListener();
		setHistoryIndexListener();
		
		currentURL.textProperty().addListener(fieldListener);
		WebController.engine.locationProperty().addListener(engineListener);
		engine.getHistory().currentIndexProperty().addListener(HistoryIndexListener);
		
		if (!PropertiesManager.manualCheckUpdate)
			UpdateDialog.checkForUpdate();
		updateCheckNSet();
		
	}
	
	/**
	 * Listener who sets the url TextField according to the current page in the WebEngin.
	 */
	private void setEngineListener() {
		engineListener = new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				currentURL.textProperty().removeListener(fieldListener);
				currentURL.setText(newValue);
				currentURL.textProperty().addListener(fieldListener);
			}
		};
	}
	
	/**
	 * Listener who loads a page according to the address entered in the url TextField.
	 */
	private void setFieldListener() {
		fieldListener = new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				engine.locationProperty().removeListener(engineListener);
				if (isGoButton) {
					engine.load(newValue);
					isGoButton = false;
				}
				engine.locationProperty().addListener(engineListener);
			}
		};
	}
	
	/**
	 * Listener who manages the previous / next buttons depending on the history entries.
	 */
	private void setHistoryIndexListener() {
		HistoryIndexListener = new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				indexHistoryValue = (int) newValue + 1;
				
				if (indexHistoryValue > 1)
					PrevButton.setDisable(false);
				if (indexHistoryValue < HistoryEntries.size())
					NextButton.setDisable(false);
			}
		};
	}
	
	/**
	 * Loads the home page by pressing the Home button.
	 */
	@FXML
	private void HomeAction() {
		engine.load(PropertiesManager.HOME_URL);
	}
	
	/**
	 * Gets the address entered in the url TextField and load it.
	 */
	@FXML
	private void GoAction() {
		isGoButton = true;
		//have to treat url...
		String urlAction = currentURL.getText();
		engine.load(urlAction);
	}
	
	/**
	 * Goes to the previous page according to the WebEngine history.
	 */
	@FXML
	private void PrevAction() {
		if (engine.getHistory().getCurrentIndex() == 1) {
			engine.getHistory().go(-1);
			PrevButton.setDisable(true);
		}
		else {
			engine.getHistory().go(-1);
		}
	}
	
	/**
	 * Goes to the next page according to the WebEngine history.
	 */
	@FXML
	private void NextAction() {
		if (indexHistoryValue == HistoryEntries.size()-1) {
			engine.getHistory().go(+1);
			NextButton.setDisable(true);
		}
		else if (indexHistoryValue < HistoryEntries.size()) {
			engine.getHistory().go(+1);
		}
	}
	
	/**
	 * Loads the entered address in the url TextField by pressing Enter (if TextField focused)
	 */
	@FXML
	private void setFieldAction() {
		currentURL.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				//have to treat url...
				String urlAction = currentURL.getText();
				engine.load(urlAction);
			}
		});
	}
	
	/**
	 * Calls the OptionsDialog class to open the Options Dialog
	 * and gives the (Home) tab to focus.
	 */
	@FXML
	private void openHomePageDialog() {
		int i = OptionsDialog.HOME_TAB;
		new OptionsDialog().setOptionsDial(i);;
	}
	
	/**
	 * Calls the OptionsDialog class to open the Options Dialog
	 * and gives the (Advanced) tab to focus.
	 */
	@FXML
	private void openAdvancedDialog() {
		int i = OptionsDialog.ADVANCED_TAB;
		new OptionsDialog().setOptionsDial(i);;
	}
	
	/**
	 * Calls the AboutDialog class to open the About Dialog.
	 */
	@FXML
	private void openAboutDialog() {
		aboutDial = new AboutDialog();
		aboutDial.setAboutDial();
	}
	
	/**
	 * Calls the UpdateDialog class to open the Update Dialog.
	 */
	@FXML
	private void openUpdateDialog() {
		updateDial = new UpdateDialog();
		updateDial.setUpdateDialog();
	}
	
	/**
	 * Reload the current page (in WebEngine, not the address in url TextField).
	 */
	@FXML
	private void reloadButtonAction() {
		WebController.engine.reload();
	}
	
	/**
	 * If there is a more recent version, gives some visual effects.
	 */
	private void updateCheckNSet() {
		if (UpdateDialog.toUpdate) {
			OptionsButton.setText("≡ !");
			OptionsButton.setPrefWidth(58);
			OptionsButton.setTextFill(Paint.valueOf("#FF0000"));
			updateLab.setVisible(true);
		}
	}
}
